/**
 * Created by W10-PRO on 9/12/2016.
 */
public class DividebyZero {
    public static void main(String[] args){
        System.out.println("20.0 / 0.0 = " + (20.0 / 0.0));         // Infinity
        System.out.println("2321.0 % 0.0 = " + (2321.0 % 0.0));     // NaN (not a number)
        System.out.println("321 / 0 = " + (321 / 0));               // ERROR
        System.out.println("321 % 0 = " + (321 % 0));               // ERROR
    }
}
