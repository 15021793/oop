/**
 * Created by W10-PRO on 9/12/2016.
 */
public class RandomWalk {

    public static void main(String[] args) {
        int n = Integer.parseInt(args[0]);
        StdDraw.setXscale(-n, +n);
        StdDraw.setYscale(-n, +n);
        StdDraw.clear(StdDraw.GRAY);
        StdDraw.enableDoubleBuffering();

        int x = 0, y = 0;
        int steps = 0;
        int r = 0; int i = 1; int j = 1;
        int a[] = {-1,-1,1,1};
        while (Math.abs(x) < n && Math.abs(y) < n) {
            StdDraw.setPenColor(StdDraw.WHITE);
            StdDraw.filledSquare(x, y, 0.45);

            for (int u=1; u <=i; ++u){

                if (r % 2 == 0) y = y + a[r];
                else x = x + a[r];
                steps++;
                StdDraw.setPenColor(StdDraw.BLUE);
                StdDraw.filledSquare(x, y, 0.45);
                StdDraw.show();
                StdDraw.pause(40);
            }
            ++j; if (j > 2) { ++i; j = 1;}
            ++r; if (r > 3) r = 0;
        }
        StdOut.println("Total steps = " + steps);
    }

}
