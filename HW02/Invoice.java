/**
 * Created by W10-PRO on 9/21/2016.
 */
public class Invoice {
    private String partNumber;
    private String partDesc;
    private int quantity;
    private double price;

    public Invoice(String n, String d, int q, double p){
        partNumber = n;
        partDesc = d;
        if (q > 0) quantity = q;
        if (p > 0.0) price = p;
    }

    public void setQuantity(int n){
        quantity = n;
    }

    public void setPrice(double n){
        price = n;
    }

    public int getQuantity() {
        return quantity;
    }

    public double getPrice() {
        return price;
    }

    public double getInvoiceAmount(){
        return (price * quantity);
    }
}
