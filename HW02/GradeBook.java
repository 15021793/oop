// Fig. 3.10: GradeBook.java
// GradeBook class with GradeBook.a constructor to initialize the course name.
public class GradeBook {
    private String courseName; // course name for this GradeBook
    private String courseInstructor;

    // constructor initializes courseName with String supplied as argument
    public GradeBook(String name, String name2) {
        courseName = name; // initializes courseName} // end constructor
        courseInstructor = name2;
        // method to set the course name
    }

    public void setCourseName(String name) {
        courseName = name; // store the course name
    } // end method setCourseName

    public void setCourseInstructor(String name) {
        courseInstructor = name;
    }

    // method to retrieve the course name
    public String getCourseName() {
        return courseName;
    } // end method getCourseName

    public String getCourseInstructor() {
        return courseInstructor;
    }
    // display GradeBook.a welcome message to the GradeBook user

    public void displayMessage() {
        // this statement calls getCourseName to get the
        // name of the course this GradeBook represents
        System.out.printf("Welcome to the grade book for\n%s!\n", getCourseName());
        System.out.printf("This course is presented by:\n%s!\n", getCourseInstructor());
    } // end method displayMessage

} // end class GradeBook

