/**
 * Created by W10-PRO on 9/21/2016.
 */
public class SavingsAccount {
    static double annualInterestRate = 5;
    private double Balance;

    public SavingsAccount(double b){
        Balance = b;
    }

    public double calculateMonthlyInterest(){
        return (Balance * annualInterestRate / 100 / 12);
    }

    public void savingsBalance() {
        Balance = Balance + calculateMonthlyInterest();
    }

    public double getBalance(){
        return Balance;
    }

}
