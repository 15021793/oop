/**
 * Created by W10-PRO on 9/21/2016.
 */

import java.io.File;
import java.util.Scanner;
import java.util.regex.Pattern;

public class Scannertest {
    public static void main(String[] args) {
        try {
            Scanner input = new Scanner(new File("D:\\text.txt"));

            String n = "";
            while (n != null) {
                input.useDelimiter(Pattern.compile("[^a-zA-Z]"));

                input.skip("[^a-zA-Z]*");

                System.out.printf("%s ", n);
                n = input.next();
            }

        } catch (java.io.FileNotFoundException e) {
        }
    }
}
