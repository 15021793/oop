/**
 * Created by W10-PRO on 9/21/2016.
 */
public class EmployeeTest {
    public static void main(String[] args) {
        Employee n1 = new Employee("HOX","VO",20.0);
        Employee n2 = new Employee("CHOS","OCS",30.0);

        System.out.printf("lương của thằng %s %s là %.2f \n", n1.getLastName(), n1.getFirstName(), n1.getSlary());
        System.out.printf("lương của thằng %s %s là %.2f \n", n2.getLastName(), n2.getFirstName(), n2.getSlary());
        System.out.printf("tổng lương của thằng %s và %s là %.2f \n", n1.getFirstName(), n2.getFirstName(), n1.getSlary()+ n2.getSlary());

        System.out.printf("lương của thằng %s %s được tăng 10percent là %.2f \n", n1.getLastName(), n1.getFirstName(), n1.get10percent());
        System.out.printf("lương của thằng %s %s được tăng 10percent là %.2f \n", n2.getLastName(), n2.getFirstName(), n2.get10percent());
    }
}
