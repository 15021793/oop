/**
 * Created by W10-PRO on 9/21/2016.
 */
public class Employee {
    private String firstName;
    private String lastName;
    private double slary;

    public Employee(String f, String l, double s) {
        firstName = f;
        lastName = l;
        if (s > 0.0) slary = s;
    }

    public void setFirstName(String n) {
        firstName = n;
    }

    public void setLastName(String n){
        lastName = n;
    }

    public void setSlary(double n) {
        if (n < 0) slary = 0.0;
        slary = n;
    }

    public String getFirstName(){
        return firstName;
    }

    public String getLastName(){
        return lastName;
    }

    public double getSlary(){
        return slary;
    }

    public double get10percent(){
        return (slary * 1.1);
    }
}
