/**
 * Created by W10-PRO on 9/21/2016.
 */
public class SavingsAccoutTest {
    public static void main(String[] args) {
        SavingsAccount saver1 = new SavingsAccount(2000.0);
        SavingsAccount saver2 = new SavingsAccount(3000.0);

        saver1.savingsBalance();
        saver2.savingsBalance();

        System.out.println(saver1.getBalance());
        System.out.println(saver2.getBalance());
    }
}
