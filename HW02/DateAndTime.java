/**
 * Created by W10-PRO on 9/22/2016.
 */
public class DateAndTime {
    private int second;
    private int month; // 1-12
    private int day;   // 1-31 based on month
    private int year;  // any year

    public DateAndTime( int h, int m, int s, int theMonth, int theDay, int theYear )
    {
        setTime(h, m, s);
        month = checkMonth( theMonth ); // validate month
        year = checkYear( theYear ); // could validate year
        day = checkDay( theDay ); // validate day

        System.out.printf(
                "Date object constructor for date %s\n", this );
    } // end Date constructor

    private int checkYear(int testYear){
        if (testYear > 1) return testYear;
        else {
            System.out.printf(
                    "Invalid month (%d) set to 1.", testYear );
            return 1;
        }
    }
    // utility method to confirm proper month value
    private int checkMonth( int testMonth )
    {
        if ( testMonth > 0 && testMonth <= 12 ) // validate month
            return testMonth;
        else // month is invalid
        {
            System.out.printf(
                    "Invalid month (%d) set to 1.", testMonth );
            return 1; // maintain object in consistent state
        } // end else
    } // end method checkMonth

    // utility method to confirm proper day value based on month and year
    private int checkDay( int testDay ) {
        int daysPerMonth[] =
                { 0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

        // check if day in range for month
        if ( testDay > 0 && testDay <= daysPerMonth[ month ] )
            return testDay;

        // check for leap year
        if ( month == 2 && testDay == 29 && ( year % 400 == 0 ||
                ( year % 4 == 0 && year % 100 != 0 ) ) )
            return testDay;

        System.out.printf( "Invalid day (%d) set to 1.", testDay );
        return 1;
    }

    public void nexTDay() {
        ++day;

        int daysPerMonth[] =
                { 0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

        if ( month == 2 && day > 29 && ( year % 400 == 0 || ( year % 4 == 0 && year % 100 != 0 ) ) )  {
            nexTMonth();
            return;
        }

        if ( day > daysPerMonth[ month ] ){
            day = 1;
            nexTMonth();
        }
    }

    public void nexTMonth() {
        ++month;
        if (month > 12) {
            month = 1;
            nexTYear();
        }
    }

    public void nexTYear(){
        ++year;
    }

    public DateAndTime(DateAndTime time) {
        // invoke Time2 three-argument constructor
        time.getHour();
        time.getMinute();
        time.getSecond();
    } // end Time2 constructor with a Time2 object argument

    // Set Methods
    // set a new time value using universal time; ensure that
    // the data remains consistent by setting invalid values to zero
    public void setTime(int h, int m, int s) {
        setHour(h);   // set the hour
        setMinute(m); // set the minute
        setSecond(s); // set the second
    } // end method setTime


    // validate and set hour
    public void setHour(int h) {
        if (h < 0 || h >= 24) {
            System.out.printf( "Invalid hour (%d) set to 0.", h );
            h = 0;
        }
        second = ((h >= 0 && h < 24) ? h : 0) * 3600 + getMinute() * 60 + getSecond();
    } // end method setHour

    // validate and set minute
    public void setMinute(int m) {
        if (m < 0 || m >=60) {
            System.out.printf( "Invalid minute (%d) set to 0.", m );
            m = 0;
        }
        second = ((m >= 0 && m < 60) ? m : 0) * 60 + getHour() * 3600 + getSecond();
    } // end method setMinute

    // validate and set second
    public void setSecond(int s) {
        if (s < 0 || s >= 60 ) {
            System.out.printf( "Invalid second (%d) set to 0.", s );
            s = 0;
        }
        second = ((s >= 0 && s < 60) ? s : 0) + getHour() * 3600 + getMinute() * 60;
    } // end method setSecond

    public void tick(){
        ++second;
        if (getSecond() == 60) incrementMinute();
    }

    public void incrementMinute() {
        second = second + 60;
        if (getMinute() == 60) incrementHour();
    }

    public void incrementHour() {
        second = second + 3600;
        if (getHour() >= 24) second = 1;
        nexTDay();
    }

    // Get Methods
    // get hour value
    public int getHour() {
        return second / 3600;
    } // end method getHour

    // get minute value
    public int getMinute() {
        return (second % 3600) / 60;
    } // end method getMinute

    // get second value
    public int getSecond() {
        return (second % 3600) % 60;
    } // end method getSecond

    public String toUniversalString() {
        return String.format("%02d:%02d:%02d \n%d/%d/%d", getHour(), getMinute(), getSecond(), month, day, year );
    } // end method toUniversalString

    // return a String of the form month/day/year
    public String toString()
    {
        return String.format( "%d:%02d:%02d %s \n%d/%d/%d", ((getHour() == 0 || getHour() == 12) ? 12 : getHour() % 12),
                getMinute(), getSecond(), (getHour() < 12 ? "AM" : "PM") , month, day, year );
    } // end method toString
}
