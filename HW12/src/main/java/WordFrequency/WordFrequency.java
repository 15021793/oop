package WordFrequency;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * Created by W10-PRO on 12/8/2016.
 */
public class WordFrequency {
    private static final String FILENAME = "D:\\oop\\HW12\\text.txt";

    public static void main(String[] args) {
        Map<String, Integer> map = new HashMap<String, Integer>();
        try {
            int max = 0; String bestWord = "";

            Scanner text = new Scanner(new File(FILENAME));
            while (text.hasNext()) {
                String word = text.next();

                if (map.containsKey(word)) {
                    int count = map.get(word) + 1;
                    if (count > max) {
                        max = count;
                        bestWord = word;
                    }

                    map.put(word,count);
                }
                else map.put(word,1);
            }

            System.out.println(bestWord + " " + max);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
