package TopTransactions;

import java.util.Comparator;

/**
 * Created by W10-PRO on 12/8/2016.
 */
public class MyComparator implements Comparator<Transactions> {
    public int compare(Transactions o1, Transactions o2) {
        if (o1.getNumber() > o2.getNumber()) return -1;
        else if (o1.getNumber() < o2.getNumber()) return 1;
        return 0;
    }
}
