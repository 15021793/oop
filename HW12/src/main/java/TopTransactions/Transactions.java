package TopTransactions;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by W10-PRO on 12/8/2016.
 */
public class Transactions {
    private String name;
    private Date date;
    private double number;

    public Transactions(String name, String date, double number) {
        this.name = name;
        try {
            this.date = new SimpleDateFormat("mm/dd/yyyyy").parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        this.number = number;
    }

    public double getNumber() {
        return number;
    }

    public void display(){
        System.out.println(name + "   " + new SimpleDateFormat("mm/dd/yyyyy").format(date) + "  " + number);
    }
}
