package TopTransactions;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.PriorityQueue;
import java.util.Scanner;

/**
 * Created by W10-PRO on 12/8/2016.
 */
public class TopTransactions {
    private static final String FILENAME = "D:\\oop\\HW12\\src\\main\\resources\\text2.txt";

    public static void main(String[] args) {
        MyComparator comparator = new MyComparator();
        PriorityQueue<Transactions> queue = new PriorityQueue<Transactions>(1000000,comparator);
        try {
            Scanner text = new Scanner(new File(FILENAME));


            while (text.hasNext()) {
                queue.add(new Transactions(text.next(),text.next(), Double.valueOf(text.next())));
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

       while (queue.size()!=0) {
           queue.poll().display();
       }
    }
}
