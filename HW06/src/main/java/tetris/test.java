package tetris;

/**
 * Created by W10-PRO on 10/12/2016.
 */
public class test {

    public static void print(TPoint[] t){
        for (int i=0; i<=3; ++i)
            System.out.println(t[i].x + " " + t[i].y);
        System.out.println();
    }

    public static void print_skirt(int[] t,int x){
        for (int i=0; i<x; ++i)
            System.out.println(t[i]);
        System.out.println();
    }


    public static void main(String[] args) {
        Board b;
        Piece pyr1, pyr2, pyr3, pyr4, s, sRotated,L1,L1_2,L1_3;

        b = new Board(3, 6);

        pyr1 = new Piece(Piece.PYRAMID_STR);
        pyr2 = pyr1.computeNextRotation();
        pyr3 = pyr2.computeNextRotation();
        pyr4 = pyr3.computeNextRotation();

    //    s = new Piece(Piece.S1_STR);
    //    sRotated = s.computeNextRotation();


        b.place(pyr1, 0, 0);
        b.commit();
        b.place(pyr1, 0, 2);
        b.clearRows();
        for (int i = 0; i< b.getWidth(); ++i) {
            for (int j = 0; j < b.getHeight(); ++j)
                System.out.print(b.getGrid(i, j) + " ");
            System.out.println();
        }
        System.out.println(b.getColumnHeight(0));
        System.out.println(b.getColumnHeight(1));
        System.out.println(b.getColumnHeight(2));
        System.out.println(b.getMaxHeight());


      /*  for (int i = 0; i< b.getWidth(); ++i) {
            for (int j = 0; j < b.getHeight(); ++j)
                System.out.print(b.getGrid(i,j)+ " ");
            System.out.println();
        } */
     /*   b.commit();
        L1 = new Piece(Piece.L1_STR);
        L1_2 = L1.computeNextRotation();
        L1_3 = L1_2.computeNextRotation();
        b.place(pyr4,0,1);
        b.clearRows();
        b.commit();
        System.out.println(b.place(L1_3,2,0));

        b.clearRows();
        print(L1_3.getBody());

        System.out.println(b.dropHeight(L1_3,0));
        System.out.println(b.dropHeight(L1_3,1));
        System.out.println(b.dropHeight(L1_3,2));

        System.out.println(b.getColumnHeight(0));
        System.out.println(b.getColumnHeight(1));
        System.out.println(b.getColumnHeight(2)); */



    }
}
