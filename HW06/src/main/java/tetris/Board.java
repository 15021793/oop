// Board.java
package tetris;
/**
 CS108 Tetris Board.
 Represents a Tetris board -- essentially a 2-d grid
 of booleans. Supports tetris pieces and row clearing.
 Has an "undo" feature that allows clients to add and remove pieces efficiently.
 Does not do any drawing or have any idea of pixels. Instead,
 just represents the abstract 2-d board.
*/
public class Board	{
	// Some ivars are stubbed out for you:
	private int width;
	private int height;
	private boolean[][] grid;
	private boolean DEBUG = true;
	boolean committed;
    private int[] widths;
    private int[] heights;
    private int maxheight;
    private boolean[][] copy_grid;
	private int[] copy_widths;
	private int[] copy_heights;
	private int copy_maxheight;
	
	
	// Here a few trivial methods are provided:
	
	/**
	 Creates an empty board of the given width and height
	 measured in blocks.
	*/
	public Board(int width, int height) {
		this.width = width;
		this.height = height;
		grid = new boolean[width][height];
		committed = true;

        widths = new int[height];
        heights = new int[width];
        maxheight = 0;

        copy_grid = new boolean[width][height];
		copy_heights = new int[width];
		copy_widths = new int[height];
		copy_maxheight = 0;
		// YOUR CODE HERE
	}
	
	
	/**
	 Returns the width of the board in blocks.
	*/
	public int getWidth() {
		return width;
	}
	
	
	/**
	 Returns the height of the board in blocks.
	*/
	public int getHeight() {
		return height;
	}
	
	
	/**
	 Returns the max column height present in the board.
	 For an empty board this is 0.
	*/
	public int getMaxHeight() {
       return maxheight;
		  // YOUR CODE HERE
	}
	
	
	/**
	 Checks the board for internal consistency -- used
	 for debugging.
	*/
	public void sanityCheck() {
		if (!DEBUG) {
		    int[] check_heights = new int[width];
            int[] check_widths = new int[height];
            int check_max = 0;
            boolean check_res = true;

            for (int i=0; i < width; ++i)
                for (int j=0; j < height; ++j)
                    if (grid[i][j]) {
                        ++check_widths[j];
                        if (j +1  > check_heights[i]) {
                            check_heights[i] = j + 1;
                            if (j + 1 > check_max) check_max = j + 1;
                        }
                    }

            for (int i=0; i < width; ++i)
                if (check_heights[i] != heights[i]) check_res = false;
          //  System.out.println(check_res);

            for (int i=0; i < height; ++i)
                if (check_widths[i] != widths[i]) check_res = false;
          //  System.out.println(check_res);

            if (check_max != maxheight) check_res = false;
          //  System.out.println(check_res);

            if (!check_res) throw new RuntimeException("description");
			// YOUR CODE HERE
		}
	}
	
	/**
	 Given a piece and an x, returns the y
	 value where the piece would come to rest
	 if it were dropped straight down at that x.
	 
	 <p>
	 Implementation: use the skirt and the col heights
	 to compute this fast -- O(skirt length).
	*/
	public int dropHeight(Piece piece, int x) {
		int y = 0;
        int[] skirt = piece.getSkirt();

        for (int i=0; i< piece.getWidth(); ++i) {
            int new_x = x + i;

            if ( y < heights[new_x] - skirt[i]) y = heights[new_x] - skirt[i];
			System.out.println(y + " " + skirt[i]);
		}
	    return y; // YOUR CODE HERE
	}
	
	
	/**
	 Returns the height of the given column --
	 i.e. the y value of the highest block + 1.
	 The height is 0 if the column contains no blocks.
	*/
	public int getColumnHeight(int x) {
		return heights[x]; // YOUR CODE HERE
	}
	
	
	/**
	 Returns the number of filled blocks in
	 the given row.
	*/
	public int getRowWidth(int y) {
		 return widths[y]; // YOUR CODE HERE
	}
	
	
	/**
	 Returns true if the given block is filled in the board.
	 Blocks outside of the valid width/height area
	 always return true.
	*/
	public boolean getGrid(int x, int y) {
		return grid[x][y]; // YOUR CODE HERE
	}
	
	
	public static final int PLACE_OK = 0;
	public static final int PLACE_ROW_FILLED = 1;
	public static final int PLACE_OUT_BOUNDS = 2;
	public static final int PLACE_BAD = 3;
	
	/**
	 Attempts to add the body of a piece to the board.
	 Copies the piece blocks into the board grid.
	 Returns PLACE_OK for a regular placement, or PLACE_ROW_FILLED
	 for a regular placement that causes at least one row to be filled.
	 
	 <p>Error cases:
	 A placement may fail in two ways. First, if part of the piece may falls out
	 of bounds of the board, PLACE_OUT_BOUNDS is returned.
	 Or the placement may collide with existing blocks in the grid
	 in which case PLACE_BAD is returned.
	 In both error cases, the board may be left in an invalid
	 state. The client can use undo(), to recover the valid, pre-place state.
	*/
	public int place(Piece piece, int x, int y) {
		// flag !committed problem
		if (!committed) throw new RuntimeException("place commit problem");

		if (committed) copy_commit();

			
		int result = PLACE_OK;

        TPoint[] body = piece.getBody();

        for (int i=0; i< 4; ++i) {
            int new_x = x + body[i].x;
            int new_y = y + body[i].y;
            if (new_x >= width || new_y >= height || new_x < 0 || new_y < 0) return PLACE_OUT_BOUNDS;
        }

        for (int i=0; i< 4; ++i) {
            int new_x = x + body[i].x;
            int new_y = y + body[i].y;

            if (grid[new_x][new_y]) return PLACE_BAD;
        }

        for (int i=0; i< 4; ++i) {
            int new_x = x + body[i].x; int new_y = y + body[i].y;

            grid[new_x][new_y] = true;

            ++widths[new_y]; if (widths[new_y] == width ) result = PLACE_ROW_FILLED;

            if (new_y + 1 > heights[new_x]) {
                heights[new_x] = new_y +1;
                if (heights[new_x] > maxheight) maxheight = heights[new_x];
            }
        }


        committed = false;

        sanityCheck();
		// YOUR CODE HERE
		
		return result;
	}
	
	
	/**
	 Deletes rows that are filled all the way across, moving
	 things above down. Returns the number of rows cleared.
	*/
	public int clearRows() {

		int rowsCleared = 0;
		// YOUR CODE HERE
        for (int i=0; i< maxheight; ++i)
            if (widths[i] == width) ++rowsCleared;
            else {
                widths[i - rowsCleared] = widths[i];
                for (int j = 0; j < width; ++j)
                    grid[j][i - rowsCleared] = grid[j][i];
            }

        for (int i = 0; i < width; ++i) heights[i] = 0;

        for (int i = maxheight -1; i >= maxheight - rowsCleared; --i) {
            widths[i] = 0 ;
            for (int j=0; j < width; ++j)
                grid[j][i] = false;
        }

        maxheight = 0;
        for (int i=0; i < width; ++i)
            for (int j=0; j < height; ++j)
                if (grid[i][j]) {
                    if (j +1  > heights[i]) {
                        heights[i] = j + 1;
                        if (heights[i] > maxheight) maxheight = j + 1;
                    }
                }

        /*
        for (int i = 0; i< getWidth(); ++i) {
            for (int j = 0; j < getHeight(); ++j)
                System.out.print(getGrid(i,j)+ " ");
            System.out.println();
        }
       */
        committed = false;
		sanityCheck();
		return rowsCleared;
	}

	private void copy_commit() {
		for (int i=0; i < width; ++i)
			System.arraycopy(grid[i],0,copy_grid[i],0,height);
		System.arraycopy(widths,0,copy_widths,0,height);
		System.arraycopy(heights,0,copy_heights,0,width);
		copy_maxheight = maxheight;
	}


	/**
	 Reverts the board to its state before up to one place
	 and one clearRows();
	 If the conditions for undo() are not met, such as
	 calling undo() twice in a row, then the second undo() does nothing.
	 See the overview docs.
	*/
	public void undo() {
		if (committed) return;

		for (int i=0; i < width; ++i)
			System.arraycopy(copy_grid[i],0,grid[i],0,height);
		System.arraycopy(copy_widths,0,widths,0,height);
		System.arraycopy(copy_heights,0,heights,0,width);
		maxheight = copy_maxheight;

		commit();

        sanityCheck();

	    // YOUR CODE HERE
	}
	
	
	/**
	 Puts the board in the committed state.
	*/
	public void commit() {
		committed = true;
	}


	
	/*
	 Renders the board state as a big String, suitable for printing.
	 This is the sort of print-obj-state utility that can help see complex
	 state change over time.
	 (provided debugging utility) 
	 */
	public String toString() {
		StringBuilder buff = new StringBuilder();
		for (int y = height-1; y>=0; y--) {
			buff.append('|');
			for (int x=0; x<width; x++) {
				if (getGrid(x,y)) buff.append('+');
				else buff.append(' ');
			}
			buff.append("|\n");
		}
		for (int x=0; x<width+2; x++) buff.append('-');
		return(buff.toString());
	}
}


