package tetris;

import javax.swing.*;
import java.awt.*;
import java.util.Currency;
import java.util.Random;

/**
 * Created by W10-PRO on 11/9/2016.
 */
public class JBrainTetris extends JTetris {
    private Brain brain;
    Brain.Move Bmove;
    int currentCount;
    protected JLabel ok;
    Piece worstCase;

    protected JCheckBox brainMode;
    protected JSlider adversary;
    /**
     * Creates a new JTetris where each tetris square
     * is drawn with the given number of pixels.
     *
     * @param pixels
     */

    JBrainTetris(int pixels) {
        super(pixels);
        brain = new DefaultBrain();
        Bmove = new Brain.Move();
        currentCount = -1;

    }

    @Override
    public Piece pickNextPiece() {
      //  int res_random = random.nextInt(100);

        if ((int) Math.random() * 100 >= adversary.getValue()){
            ok.setText("ok");
            return super.pickNextPiece();
        }
        else {
            double score = 0;
            Brain.Move currentMove = new Brain.Move();

            for (Piece i : pieces) {
                currentMove = brain.bestMove(board,i,HEIGHT, currentMove);

                if (currentMove == null) continue;

                if (score < currentMove.score) {
                    worstCase = currentMove.piece;
                    score = currentMove.score;
                }
            }

            ok.setText("*ok*");
        }


        return worstCase;
    }

    @Override
    public void tick(int verb) {

        if (verb == DOWN && brainMode.isSelected()) {
            if (currentCount != count) {
                currentCount = count;
                board.undo();
                Bmove = brain.bestMove(board,currentPiece,HEIGHT,Bmove);
            }

            if (Bmove != null) {
                if (!Bmove.piece.equals(currentPiece)) super.tick(ROTATE);

                if (Bmove.x < currentX) super.tick(LEFT);
                else if (Bmove.x > currentX) super.tick(RIGHT);
            }
        }

        super.tick(verb);
    }

    @Override
    public JComponent createControlPanel() {

        JComponent panel = super.createControlPanel();

        panel.add(new JLabel("Brain:"));
        brainMode = new JCheckBox("Brain active");
        panel.add(brainMode);

        panel.add(new JLabel("Adversary:"));
        adversary = new JSlider(0, 100, 0); // min, max, current
        adversary.setPreferredSize(new Dimension(100,15));
        panel.add(adversary);

        ok = new JLabel("ok");
        panel.add(ok);


        return panel;
    }

    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception ignored) { }

        JBrainTetris tetris = new JBrainTetris(16);
        JFrame frame = JBrainTetris.createFrame(tetris);
        frame.setVisible(true);
    }

}
