package BankAcc;

/**
 * Created by W10-PRO on 10/10/2016.
 */
public class BankTest {
    public static void main(String[] args) {
        FlatFee a = new FlatFee(15000);
        NickelNDime b = new NickelNDime(50000);
        Gambler c = new Gambler(70000);

        try {
            System.out.println(a.withdraw(32132131));
            System.out.println(a.deposit(12));
            System.out.println(b.withdraw(5000));
            System.out.println(c.withdraw(5000));
            System.out.println(c.withdraw(4000));
            System.out.println(c.withdraw(6000));

            System.out.println(a.getBalance());
            System.out.println(b.getBalance());
            System.out.println(c.getBalance());


            a.endMonth();
            b.endMonth();
            c.endMonth();
        } catch (OverDrawException e) {

        } catch (InvalidAmountException e) {
        }
    }
}
