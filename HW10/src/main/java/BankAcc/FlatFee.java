package BankAcc;

/**
 * Created by W10-PRO on 10/10/2016.
 */
public class FlatFee extends BankAccount{
    public FlatFee (int n){
        super(n);
    }

    public boolean withdraw(int n) throws InvalidAmountException, OverDrawException {
        return super.withdraw(n);
    }

    public int endMonthCharge(){
        return 10000;
    }

}
