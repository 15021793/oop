package stacks;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by W10-PRO on 10/25/2016.
 */
public class Sarray<T> implements StackOfString<T> {

    private int size;
    private T[] list;

    public Sarray(int capacity) {
        list = (T[]) new Object[capacity];
        size = 0;
    }

    @Override
    public boolean isEmpty() {
        return (size == 0);
    }

    @Override
    public void push(T s) {
        ++size;
        list[size] = s;
    }

    @Override
    public T pop() {
        T s = list[size];
        list[size] = null;
        --size;
        return s;
    }

    @Override
    public Iterator<T> interator() {
        return new ReverseArrayIterator();
    }

    private class ReverseArrayIterator implements Iterator {
        private int i = size;

        @Override
        public boolean hasNext() {
            return i > 0;
        }

        @Override
        public Object next() {
            return list[--i];
        }
    }
}
