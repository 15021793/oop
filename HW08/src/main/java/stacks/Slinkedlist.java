package stacks;

import java.util.ListIterator;

/**
 * Created by W10-PRO on 10/25/2016.
 */
public class Slinkedlist<T> implements StackOfString<T> {

    private class Node{
        T string;
        Node next;

        private Node(T s, Node n){
            string = s;
            next = n;
        }
    }

    int size;
    private Node head = null;

    public Slinkedlist(){
        size = 0;
        head = null;
    }

    public boolean isEmpty(){
        return (size == 0);
    }

    public void push(T s){
        if (size == 0) {
            ++size;
            head = new Node(s,null);
            return;
        }
        ++size;
        Node tmp = new Node(s,head);
        head = tmp;
        return;
    }

    public T pop(){
        T tmp = head.string;
        head = head.next;
        --size;
        return tmp;
    }

    @Override
    public Iterator<T> interator() {
        return new ListIterator();
    }

    private class ListIterator implements Iterator<T> {
        private Node current = head;


        @Override
        public boolean hasNext() {
            return (current != null);
        }

        @Override
        public T next() {
            T item = current.string;
            current = current.next;
            return item;
        }
    }
}
