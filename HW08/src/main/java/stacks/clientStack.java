package stacks;

/**
 * Created by W10-PRO on 11/9/2016.
 */
public class clientStack {
    public static void main(String[] args) {
        Slinkedlist<String> stack = new Slinkedlist<>();
        stack.push("M");
        stack.push("I");
        stack.push("N");
        stack.push("H");
        stack.push("Dz");

        Iterator<String> i = stack.interator();
        while (i.hasNext()) {
            String s = i.next();
            System.out.print(s + " ");
        }
        System.out.print("\n");

        Sarray<Integer> stack2 = new Sarray<>(6);
        stack2.push(1);
        stack2.push(2);
        stack2.push(5);
        stack2.push(3);
        stack2.push(4);
    }
}
