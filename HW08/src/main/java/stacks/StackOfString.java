package stacks;

/**
 * Created by W10-PRO on 10/25/2016.
 */
public interface StackOfString<T> extends Iterable<T> {
    boolean isEmpty();
    void push(T s);
    T pop();
}
