package stacks;

/**
 * Created by W10-PRO on 11/9/2016.
 */
public interface Iterable<T> {
    Iterator<T> interator();
}
