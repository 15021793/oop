/**
 * Created by W10-PRO on 10/25/2016.
 */
public class InsertionSort<K extends Comparable> {
    public void sort(K[] account, MyComparator<K> compare) {
        int j;
        for (int i=1; i<=account.length-1; ++i) {
            j = i;
            while (j >=1 && !compare.less(account[j-1],account[j])) {
                K tmp;
                tmp = account[j-1];
                account[j-1] = account[j];
                account[j] = tmp;
                --j;
            }
        }
    }

}
