/**
 * Created by W10-PRO on 10/25/2016.
 */
public interface MyComparator<K extends  Comparable>{
    boolean less(K a1,K a2);
}
