/**
 * Created by W10-PRO on 10/25/2016.
 */
public class DemoClient {
    public static void main(String[] args) {
        Integer[] account = {4,3,4,1,5};

        MyComparator compare = new MyComparator() {
            @Override
            public boolean less(Comparable a1, Comparable a2) {
                return (a1.compareTo(a2) < 0);
            }
        };

        InsertionSort<Integer> a = new InsertionSort<Integer>();

        a.sort(account,compare);

        for (Integer i : account)
            System.out.println(i);

    }
}
