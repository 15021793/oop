package WeatherMonitoringApplication;

/**
 * Created by W10-PRO on 12/7/2016.
 */
public interface Subject extends Observer{
    void registerObserver(Object object);
    void removeObserver(Object object);
    void notifyObserver();
}
