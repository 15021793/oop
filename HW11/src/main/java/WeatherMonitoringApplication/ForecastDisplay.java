package WeatherMonitoringApplication;

/**
 * Created by W10-PRO on 12/7/2016.
 */
public class ForecastDisplay implements Observer,DisplayElement {
    WeatherData weatherData;

    public ForecastDisplay(WeatherData weatherData) {
        this.weatherData = weatherData;
        weatherData.registerObserver(this);
    }

    public void update() {
        display();
    }

    public void display() {
        System.out.println("Have a good day!");
    }
}
