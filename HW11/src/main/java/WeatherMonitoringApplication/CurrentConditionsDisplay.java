package WeatherMonitoringApplication;

/**
 * Created by W10-PRO on 12/7/2016.
 */
public class CurrentConditionsDisplay extends WeatherData implements Observer,DisplayElement {
    WeatherData weatherData;

    public CurrentConditionsDisplay(WeatherData weatherData) {
        this.weatherData = weatherData;
        weatherData.registerObserver(this);
    }

    public void display() {
        System.out.println("Current conditions: " + weatherData.getTemperature() + "F degrees and "
                + weatherData.getHumidity() + "% humidity");
    }

    public void update() {
        display();
    }
}
