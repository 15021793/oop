package WeatherMonitoringApplication;

/**
 * Created by W10-PRO on 12/7/2016.
 */
public class StatisticsDisplay implements Observer,DisplayElement {
    WeatherData weatherData;
    private float avgTemp,maxTemp,minTemp;
    private static float sumTemp;
    private static int coutTemp = 0;

    public StatisticsDisplay(WeatherData weatherData) {
        this.weatherData = weatherData;
        weatherData.registerObserver(this);
        minTemp = 10000;
        maxTemp = -10000;
    }

    public void update() {
        ++coutTemp;
        maxTemp = Math.max(maxTemp,weatherData.getTemperature());
        sumTemp += weatherData.getTemperature();
        avgTemp = sumTemp / coutTemp;
        minTemp = Math.min(minTemp,weatherData.getTemperature());
        display();
    }

    public void display() {
        System.out.println("Avg/Max/Min temperature = " + avgTemp + "/" + maxTemp + "/" + minTemp);
    }
}
