package WeatherMonitoringApplication2;

import java.util.Observable;
import java.util.Observer;

/**
 * Created by W10-PRO on 12/7/2016.
 */
public class ForecastDisplay implements Observer,DisplayElement {
    WeatherData weatherData;

    public ForecastDisplay(WeatherData weatherData) {
        this.weatherData = weatherData;
        weatherData.registerObserver(this);
    }

    public void display() {
        System.out.println("Have a good day!");
    }

    public void update(Observable o, Object arg) {
        if (arg instanceof Observer) display();
    }
}
