package WeatherMonitoringApplication2;

/**
 * Created by W10-PRO on 12/7/2016.
 */
public interface DisplayElement {
    void display();
}
