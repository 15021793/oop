package WeatherMonitoringApplication2;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;


/**
 * Created by W10-PRO on 12/7/2016.
 */
public class WeatherData extends Observable {
    private float temp, humidity, pressure;
    private List<Observer> obs = new ArrayList<Observer>();

    public void registerObserver(Object object) {
        if (object == null) throw new NullPointerException();
        if (object instanceof Observer) obs.add((Observer) object);
    }

    public void removeObserver(Object object) {
        if (object == null) throw new NullPointerException();
        if (object instanceof Observer) obs.remove(object);
    }

    public void notifyObserver() {
        for (Observer observer : obs)
            observer.update(this,observer);
    }

    public float getTemperature() {
        return temp;
    }

    public float getHumidity() {
        return humidity;
    }

    public float getPressure() {
        return pressure;
    }


    public void setMeasurements(float temp, float humidity, float pressure) {
        this.temp = temp;
        this.humidity = humidity;
        this.pressure = pressure;

        notifyObserver();
    }

    public void update() {
        return;
    }
}
