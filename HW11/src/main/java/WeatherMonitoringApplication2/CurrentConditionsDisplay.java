package WeatherMonitoringApplication2;

import java.util.Observable;
import java.util.Observer;

/**
 * Created by W10-PRO on 12/7/2016.
 */
public class CurrentConditionsDisplay extends WeatherData implements DisplayElement,Observer {
    WeatherData weatherData;

    public CurrentConditionsDisplay(WeatherData weatherData) {
        this.weatherData = weatherData;
        weatherData.registerObserver(this);
    }

    public void display() {
        System.out.println("Current conditions: " + weatherData.getTemperature() + "F degrees and "
                + weatherData.getHumidity() + "% humidity");
    }

    public void update(Observable o, Object arg) {
        if  (arg instanceof Observer) display();
    }
}
