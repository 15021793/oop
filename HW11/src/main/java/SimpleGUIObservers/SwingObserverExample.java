package SimpleGUIObservers;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by W10-PRO on 12/8/2016.
 */
public class SwingObserverExample {
    private JFrame frame;

    public static void main(String[] args) {
        SwingObserverExample example = new SwingObserverExample();
        example.go();
    }

    private void go() {
        frame = new JFrame();

        JButton button = new JButton("Should I do it?");
        button.addActionListener(new AngelListener());
        button.addActionListener(new DevilListener());
        frame.getContentPane().add(button, BorderLayout.CENTER);

        frame.setSize(300, 200);
        frame.setVisible(true);
    }

    private class AngelListener implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            System.out.println("AAAAAAAAAAAAAAAA :( !");
        }
    }

    private class DevilListener implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            System.out.println("Come on, do it!");
        }
    }
}
