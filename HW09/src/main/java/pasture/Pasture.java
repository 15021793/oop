package pasture;

import java.awt.*;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.List;


/**
 * A pasture contains sheep, wolves, fences, plants, and possibly
 * other entities. These entities move around in the pasture and try
 * to find food, other entities of the same kind and run away from
 * possible enemies.
 */
public class Pasture {

    private int width = 20;
    private int height = 20;

    // wolves
    private int wolves = 5;
    protected  int Wolves_moveDelay;
    protected  int Wolves_liveTime;
    protected  int Wolves_produceTime;
    protected  int Wolves_TimeCanProduce;
    protected  int Wolves_Sight;
    // sheep
    private int sheep = 30;
    protected  int Sheep_moveDelay;
    protected  int Sheep_liveTime;
    protected  int Sheep_produceTime;
    protected  int Sheep_TimeCanProduce;
    protected  int Sheep_Sight;
    // plants
    private int plants = 30;
    protected  int Plants_produceTime;
    // fences
    private int fences = 20;

    private Set<Entity> world = new HashSet<Entity>();
    private Map<Point, List<Entity>> grid = new HashMap<Point, List<Entity>>();
    private Map<Entity, Point> point = new HashMap<Entity, Point>();

    private PastureGUI gui;



    /**
     * Creates a new instance of this class and places the entities in
     * it on random positions.
     */
    public Pasture() {
        readConfig();
        Engine engine = new Engine(this);
        gui = new PastureGUI(width, height, engine);

        /* The pasture is surrounded by a fence. Replace pasture.Animal for
         * Fence when you have created that class */
        for (int i = 0; i < width; i++) {
            addEntity(new Fences(this), new Point(i, 0));
            addEntity(new Fences(this), new Point(i, height - 1));
        }
        for (int i = 1; i < height - 1; i++) {
            addEntity(new Fences(this), new Point(0, i));
            addEntity(new Fences(this), new Point(width - 1, i));
        }

        /* 
         * Now insert the right number of different entities in the
         * pasture.
         */
        // sheep
        for (int i = 0; i < sheep; i++) {
            Entity sheep_1 = new Sheep(this,Sheep_moveDelay,Sheep_liveTime,Sheep_TimeCanProduce,Sheep_produceTime,Sheep_Sight);
            addEntity(sheep_1, getFreePosition(sheep_1));
        }
        // wolves
        for (int i = 0; i < wolves; i++) {
            Entity wolves_1 = new Wolves(this,Wolves_moveDelay,Wolves_liveTime,Wolves_TimeCanProduce,Wolves_produceTime,Wolves_Sight);
            addEntity(wolves_1, getFreePosition(wolves_1));
        }
        // plants
        for (int i = 0; i < plants; i++) {
            Entity grass = new GrassTufts(this, Plants_produceTime);
            addEntity(grass, getFreePosition(grass));
        }
        //fences
        for (int i = 0; i < fences; i++) {
            Entity fences_1 = new Fences(this);
            addEntity(fences_1, getFreePosition(fences_1));
        }

        gui.update();
    }

    private void readConfig() {
        Properties prop = new Properties();
        InputStream input = null;

        try {
            String filename = "config.properties";
            input = getClass().getClassLoader().getResourceAsStream(filename);
            if (input == null) {
                System.out.println("Sorry, unable to find " + filename);
                return;
            }

            prop.load(input);
            height = Integer.parseInt(prop.getProperty("world.height"));
            width = Integer.parseInt(prop.getProperty("world.width"));

            Plants_produceTime = Integer.parseInt(prop.getProperty("grass.reproductionCycle"));

            Sheep_produceTime = Integer.parseInt(prop.getProperty("sheep.reproductionCycle"));
            Sheep_TimeCanProduce = Integer.parseInt(prop.getProperty("sheep.reproductionAge"));
            Sheep_moveDelay = Integer.parseInt(prop.getProperty("sheep.movementCycle"));
            Sheep_liveTime = Integer.parseInt(prop.getProperty("sheep.timeWithoutFood"));
            Sheep_Sight = Integer.parseInt(prop.getProperty("sheep.sightOfSheep"));

            Wolves_produceTime = Integer.parseInt(prop.getProperty("wolf.reproductionCycle"));
            Wolves_TimeCanProduce = Integer.parseInt(prop.getProperty("wolf.reproductionAge"));
            Wolves_moveDelay = Integer.parseInt(prop.getProperty("wolf.movementCycle"));
            Wolves_liveTime = Integer.parseInt(prop.getProperty("wolf.timeWithoutFood"));
            Wolves_Sight = Integer.parseInt(prop.getProperty("wolf.sightOfWolves"));


        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void refresh() {
        gui.update();
    }

    /**
     * Returns a random free position in the pasture if there exists
     * one.
     * <p/>
     * If the first random position turns out to be occupied, the rest
     * of the board is searched to find a free position.
     */
    private Point getFreePosition(Entity toPlace)
            throws MissingResourceException {
        Point position = new Point((int) (Math.random() * width),
                (int) (Math.random() * height));
        int startX = (int) position.getX();
        int startY = (int) position.getY();

        int p = startX + startY * width;
        int m = height * width;
        int q = 97; //any large prime will do

        for (int i = 0; i < m; i++) {
            int j = (p + i * q) % m;
            int x = j % width;
            int y = j / width;

            position = new Point(x, y);
            boolean free = true;

            Collection<Entity> c = getEntitiesAt(position);
            if (c != null) {
                for (Entity thisThing : c) {
                    if (!toPlace.isCompatible(thisThing)) {
                        free = false;
                        break;
                    }
                }
            }
            if (free) return position;
        }
        throw new MissingResourceException(
                "There is no free space" + " left in the pasture",
                "pasture.Pasture", "");
    }


    public Point getPosition(Entity e) {
        return point.get(e);
    }

    public int getWidth() {return width;}
    public int getHeight(){return height;}

    /**
     * Add a new entity to the pasture.
     */
    public void addEntity(Entity entity, Point pos) {
        world.add(entity);

        List<Entity> l = grid.get(pos);
        if (l == null) {
            l = new ArrayList<Entity>();
            grid.put(pos, l);
        }
        l.add(entity);

        point.put(entity, pos);

        gui.addEntity(entity, pos);
    }

    public void moveEntity(Entity e, Point newPos) {

        Point oldPos = point.get(e);
        List<Entity> l = grid.get(oldPos);
        if (!l.remove(e))
            throw new IllegalStateException("Inconsistent stat in pasture.Pasture");
        /* We expect the entity to be at its old position, before we
           move, right? */

        l = grid.get(newPos);
        if (l == null) {
            l = new ArrayList<Entity>();
            grid.put(newPos, l);
        }
        l.add(e);

        point.put(e, newPos);

        gui.moveEntity(e, oldPos, newPos);
    }

    /**
     * Remove the specified entity from this pasture.
     */
    public void removeEntity(Entity entity) {

        Point p = point.get(entity);
        world.remove(entity);
        grid.get(p).remove(entity);
        point.remove(entity);
        gui.removeEntity(entity, p);

    }

    /**
     * Various methods for getting information about the pasture
     */

    public List<Entity> getEntities() {
        return new ArrayList<Entity>(world);
    }

    public Collection<Entity> getEntitiesAt(Point lookAt) {

        Collection<Entity> l = grid.get(lookAt);

        if (l == null) {
            return null;
        } else {
            return new ArrayList<Entity>(l);
        }
    }


    public Collection<Point> getFreeNeighbours(Entity entity) {
        Set<Point> free = new HashSet<Point>();
        Point p;

        int entityX = (int) getEntityPosition(entity).getX();
        int entityY = (int) getEntityPosition(entity).getY();

        for (int x = -1; x <= 1; x++) {
            for (int y = -1; y <= 1; y++) {
                p = new Point(entityX + x,
                        entityY + y);
                if (freeSpace(p, entity))
                    free.add(p);
            }
        }
        return free;
    }

    private boolean freeSpace(Point p, Entity e) {

        List<Entity> l = grid.get(p);
        if (l == null) return true;
        for (Entity old : l)
            if (!e.isCompatible(old)) return false;
        return true;
    }

    public Point getEntityPosition(Entity entity) {
        return point.get(entity);
    }


    /**
     * The method for the JVM to run.
     */
    public static void main(String[] args) {

        new Pasture();
    }


}


