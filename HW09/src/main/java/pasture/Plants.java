package pasture;

import javax.swing.*;
import java.awt.*;
import java.util.Collection;
import java.util.Iterator;

/**
 * Created by W10-PRO on 11/16/2016.
 */
public abstract class Plants implements Entity{
    protected  Pasture pasture;
    protected  boolean alive;
    protected  int produceTime;

    private  int coutTimeCanProduce;

    public Plants(Pasture pasture, int produceTime) {
        this.pasture = pasture;
        this.produceTime = produceTime;
        alive = true;
        coutTimeCanProduce = 0;
    }

    public void tick() {
        if (!alive) return;

        coutTimeCanProduce++;

        if (coutTimeCanProduce >= produceTime) ReProcude();
    }


    protected void ReProcude(){
        Point neighbour =
                (Point)getRandomMember
                        (pasture.getFreeNeighbours(this));

        if (neighbour != null) {
            Baby(neighbour);
            coutTimeCanProduce = 0;
        }
    }


    protected static <X> X getRandomMember(Collection<X> c) {
        if (c.size() == 0)
            return null;

        Iterator<X> it = c.iterator();
        int n = (int)(Math.random() * c.size());

        while (n-- > 0) {
            it.next();
        }

        return it.next();
    }


    abstract public ImageIcon getImage();
    abstract public boolean isCompatible(Entity otherEntity);
    protected abstract void Baby(Point neighbour);
}
