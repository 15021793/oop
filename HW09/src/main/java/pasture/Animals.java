package pasture;

import javax.swing.*;
import java.awt.*;
import java.util.Collection;
import java.util.Iterator;
import java.util.Random;


/**
 * Created by W10-PRO on 11/16/2016.
 */
public abstract class Animals implements Entity {
    protected  Pasture pasture;
    protected  int moveDelay;
    protected  boolean alive;
    protected  int liveTime;
    protected  int produceTime;
    protected  int TimeCanProduce;
    protected  int sight;


    private  int coutTimeCanProduce;
    private  int copy_moveDelay;
    private  int copy_liveTime;
    private  int copy_TimeCanProduce;
    private  int baseF;


    public Animals(Pasture pasture, int moveDelay, int liveTime, int TimeCanProduce, int produceTime, int sight) {
        this.pasture = pasture;
        this.moveDelay = moveDelay;
        this.liveTime = liveTime;
        this.TimeCanProduce = TimeCanProduce;
        this.produceTime = produceTime;
        this.sight = sight;

        coutTimeCanProduce = 0;
        alive = true;
        copy_liveTime = liveTime;
        copy_moveDelay = moveDelay;
        copy_TimeCanProduce = TimeCanProduce;
        baseF = 1;
    }


    public void tick(){
        if(alive) {
            copy_moveDelay--;
            copy_liveTime--;
            ++coutTimeCanProduce;
        }
        else return;

        if (check_live()) return;


        if (coutTimeCanProduce >= TimeCanProduce) ReProcude();


        if(copy_moveDelay == 0) {

            Point neighbour = MoveOfAnimal();

            if(neighbour != null) {

                if (check_food(neighbour) != null){
                    check_food(neighbour).Rip();
                    copy_liveTime = liveTime;
                    baseF = 1;
                }
                pasture.moveEntity(this, neighbour);
            }
            copy_moveDelay = moveDelay;
        }
    }


    protected int findTarget(Point firstE, Point lastE) {
        return Math.max(Math.abs(firstE.x - lastE.x), Math.abs(firstE.y - lastE.y));
    }

    protected Point MoveOfAnimal() {
        int nextBestE = -100000000;
        int nextBackup= -100000000;

        boolean okE = false;
        boolean okF = false;

        Random random = new Random();
        int u,v;

        Point first = new Point(pasture.getPosition(this));
        Point neighbour = new Point(first);
        Point neighbourBackup = new Point(first);

        Point topLeft = new Point(Math.max(0,first.x - sight),Math.max(0, first.y - sight));
        Point bottomRight = new Point(Math.min(pasture.getWidth(),first.x + sight),Math.min(pasture.getHeight(),first.y + sight));

        for (int Randomnumber =0; Randomnumber<=30; ++Randomnumber) {
            u = 1 - random.nextInt(3);
            v = 1 - random.nextInt(3);

            Point tmp_first = new Point(first.x + u,first.y + v);
            int bestE = 0; int bestF = 0;

            if (tmp_first.x > 0 && tmp_first.x < pasture.getWidth() && tmp_first.y > 0 && tmp_first.y < pasture.getHeight()
                    && check_Point(tmp_first)) {

                bestE = find_Best_enermy(tmp_first, topLeft, bottomRight); // nearest enermy , estimate from this place
                bestF = find_Best_food(tmp_first, topLeft, bottomRight); // nearest food , estimate from this place

                // run from enermy if Aminal are not hungry

                if (RunFromEnermy(bestE,nextBackup)) {
                    nextBackup = bestE;
                    neighbourBackup.x = tmp_first.x;
                    neighbourBackup.y = tmp_first.y;
                    okE = true;
                }

                // run from enermy and try to eat food when this Animal got hungry
                if (bestE > 2 && bestE - bestF > nextBestE) {
                    nextBestE = bestE - bestF;
                    neighbour.x = tmp_first.x;
                    neighbour.y = tmp_first.y;
                    okF = true;
                }

            }
        }
        try {
            if (!okE) neighbourBackup =
                    (Point) getRandomMember
                            (pasture.getFreeNeighbours(this));

            neighbour = new Point(shouldMove(neighbour, neighbourBackup, baseF, okF));
        }catch (Exception e) {
            System.out.println(e.toString());
        }

        return neighbour;
    }

    private int find_Best_food(Point tmp_first, Point topLeft, Point bottomRight) {
        int best = 100000000;

        for (int i = topLeft.x; i <= bottomRight.x; ++i)
            for (int j = topLeft.y; j <= bottomRight.y; ++j) {
                Point tmp = new Point(i,j);

                if (check_food(tmp) != null) {
                    int x =  findTarget(tmp_first, tmp);
                    if (best > x) best = x;
                }
            }

        return best;
    }

    private boolean check_Point(Point tmp) {
        Collection<Entity> c = pasture.getEntitiesAt(tmp);

        if (c != null) {
            for (Entity thisThing : c) {
                if (!isCompatible(thisThing)) return false;
            }
        }

        return true;
    }

    private int find_Best_enermy(Point tmp_first, Point topLeft, Point bottomRight) {
        int best = 100000000;

        for (int i = topLeft.x; i <= bottomRight.x; ++i)
            for (int j = topLeft.y; j <= bottomRight.y; ++j) {
                Point tmp = new Point(i,j);

                if (check_enermy(tmp)) {
                    int x =  findTarget(tmp_first, tmp);
                    if (best > x) best = x;
                }
            }

        return best;
    }


    private boolean check_enermy(Point tmp) {
        Collection<Entity> c = pasture.getEntitiesAt(tmp);

        if (c != null) {
            for (Entity thisThing : c) {
                if (isEnermy(thisThing)) {
                    return true;
                }
            }
        }

        return false;
    }

    protected abstract boolean isEnermy(Entity thisThing);


    protected boolean check_live() {
        if (copy_liveTime == 0) {
            this.Rip();
            return true;
        }

        if (copy_liveTime <= liveTime/2) baseF = 2;
        return false;
    }

    protected void ReProcude(){
        Point neighbour =
                (Point)getRandomMember
                        (pasture.getFreeNeighbours(this));

        if (neighbour != null) {
            Baby(neighbour);
            coutTimeCanProduce = TimeCanProduce - produceTime;
        }
    }


    protected Entity check_food(Point x){
        Collection<Entity> c = pasture.getEntitiesAt(x);

        if (c != null) {
            for (Entity thisThing : c) {
                if (isFood(thisThing)) {
                   // thisThing.Rip();
                    return thisThing;
                }
            }
        }

        return null;
    }



    protected static <X> X getRandomMember(Collection<X> c) {
        if (c.size() == 0)
            return null;

        Iterator<X> it = c.iterator();
        int n = (int)(Math.random() * c.size());

        while (n-- > 0) {
            it.next();
        }

        return it.next();
    }

    protected abstract void Baby(Point neighbour);
    abstract public int getCode();
    public abstract boolean isFood(Entity thisThing);
    abstract public ImageIcon getImage();
    abstract public boolean isCompatible(Entity otherEntity);
    protected abstract Point shouldMove(Point neighbour, Point neighbourBackup, int baseF, boolean okH);
    protected abstract boolean RunFromEnermy(int bestE, int nextBackup);

}
