package pasture;

import javax.swing.*;
import java.awt.*;

/**
 * Created by W10-PRO on 11/16/2016.
 */
public class Wolves extends Animals {
    private final ImageIcon image = new ImageIcon(PastureGUI.class.getResource("/wolf.gif"));

    public Wolves(Pasture pasture, int moveDelay, int liveTime, int TimeCanProduce, int produceTime, int sight) {
        super(pasture, moveDelay, liveTime, TimeCanProduce, produceTime,sight);
    }

    protected boolean RunFromEnermy(int bestE, int nextBackup) {
        return false;
    }

    protected boolean isEnermy(Entity thisThing) {
        return false;
    }


    public boolean getAlive() {
        return alive;
    }

    protected void Baby(Point neighbour) {
        pasture.addEntity(new Wolves(pasture,moveDelay,liveTime,TimeCanProduce,produceTime,sight),neighbour);
    }

    @Override
    public int getCode() {
        return code_wolves;
    }

    public boolean isFood(Entity thisThing) {
        if (thisThing.getCode() == code_sheep) return true;
        return false;
    }

    public ImageIcon getImage() {
        return image;
    }

    public boolean isCompatible(Entity otherEntity) {
        if (!otherEntity.getAlive() && otherEntity.getCode() != code_fences) return true;
        if (otherEntity.getCode() != code_fences && otherEntity.getCode() != code_wolves) return true;
        return false;
    }

    protected Point shouldMove(Point neighbour, Point neighbourBackup, int baseF, boolean okH) {
        return neighbour;
    }

    public void Rip() {
        alive = false;
        pasture.removeEntity(this);
    }
}
