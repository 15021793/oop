package pasture;

import javax.swing.*;
import java.awt.*;
import java.util.Collection;

/**
 * Created by W10-PRO on 11/16/2016.
 */
public class Sheep extends Animals{
    private final ImageIcon image = new ImageIcon(PastureGUI.class.getResource("/sheep.gif"));

    public Sheep(Pasture pasture, int moveDelay, int liveTime, int TimeCanProduce, int produceTime, int sight) {
        super(pasture, moveDelay, liveTime, TimeCanProduce, produceTime,sight);
    }

    protected boolean RunFromEnermy(int bestE, int nextBackup) {
        if (bestE != 100000000 && bestE > nextBackup) return true;
        return false;
    }

    public boolean isEnermy(Entity thisThing) {
        return  (thisThing.getCode() == code_wolves);
    }


    protected void Baby(Point neighbour) {
        pasture.addEntity(new Sheep(pasture,moveDelay,liveTime,TimeCanProduce,produceTime,sight),neighbour);
    }


    public ImageIcon getImage(){
        return image;
    }

    public boolean isCompatible(Entity otherEntity) {
        if (!otherEntity.getAlive() && otherEntity.getCode() != code_fences) return true;
        if (otherEntity.getCode() != code_sheep && otherEntity.getCode() != code_fences && otherEntity.getCode() != code_wolves) return true;
       // && otherEntity.getCode() != code_wolves
        return false;
    }

    protected Point shouldMove(Point neighbour, Point neighbourBackup, int baseF, boolean okF) {
        Point tmp = new Point(neighbour);
        if (baseF == 1 || !okF) tmp = new Point(neighbourBackup);
        return tmp;
    }


    @Override
    public int getCode() {
        return code_sheep;
    }

    public void Rip() {
        alive = false;
        pasture.removeEntity(this);
    }

    public boolean getAlive() {
        return alive;
    }


    public boolean isFood(Entity thisThing) {
        if (thisThing.getCode() == code_plants) return true;
        return false;
    }

}
