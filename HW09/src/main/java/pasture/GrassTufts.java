package pasture;

import javax.swing.*;
import java.awt.*;

/**
 * Created by W10-PRO on 11/17/2016.
 */
public class GrassTufts extends Plants{
    private final ImageIcon image = new ImageIcon(PastureGUI.class.getResource("/plant-transparent.gif"));

    public GrassTufts(Pasture pasture, int produceTime) {
        super(pasture, produceTime);
    }


    public boolean getAlive() {
        return alive;
    }

    public ImageIcon getImage() {
        return image;
    }

    public boolean isCompatible(Entity otherEntity) {
        if (!otherEntity.getAlive() && otherEntity.getCode() != code_fences) return true;
        if (otherEntity.getCode() != code_plants && otherEntity.getCode() != code_fences) return true;
        return false;
    }

    public int getCode() {
        return code_plants;
    }

    public void Rip() {
        alive = false;
        pasture.removeEntity(this);
    }


    protected void Baby(Point neighbour) {
        pasture.addEntity(new GrassTufts(pasture,produceTime),neighbour);
    }

    protected void setProduceTime() {
        produceTime = 100000;
    }
}
