package pasture;

import javax.swing.*;

/**
 * Created by W10-PRO on 11/17/2016.
 */
public class Fences implements Entity {
    private final ImageIcon image = new ImageIcon(PastureGUI.class.getResource("/fence.gif"));
    protected Pasture pasture;
    protected boolean alive;


    public Fences(Pasture pasture){
        this.pasture = pasture;
    }

    public void tick() {
        return;
    }

    public ImageIcon getImage() {
        return image;
    }

    public boolean isCompatible(Entity otherEntity) {
        return false;
    }

    public int getCode() {
        return 4;
    }

    public void Rip() {
        return;
    }

    public boolean getAlive() {
        return false;
    }
}
