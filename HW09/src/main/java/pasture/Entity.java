package pasture;

import javax.swing.*;

/**
 * This is the superclass of all entities in the pasture simulation
 * system. This interface <b>must</b> be implemented by all entities
 * that exist in the simulation of the pasture.
 */
public interface Entity {
    int code_wolves = 1;
    int code_sheep = 2;
    int code_fences = 4;
    int code_plants = 3;

    public void tick();

    /** 
     * ImageIcon returns the icon of this entity, to be displayed by
     * the pasture gui.
     */
    public ImageIcon getImage();
    
    public boolean isCompatible(Entity otherEntity);

    public int getCode();

    public void Rip();

    public boolean getAlive();

}
