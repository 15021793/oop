import people.Employee;

import  java.util.Date;
/**
 * Created by W10-PRO on 10/10/2016.
 */
public class Manager extends Employee {
    private  Employee assistant;

    public  Manager(String name, Date birthday, double salary){
        super(name,birthday,salary);
    }

    public void setAssistant(Employee s){
        assistant = s;
    }

    public String toString(){
       return (super.toString() + " " + assistant.getName() );
    }
}
