package people;

import java.util.Date;
import  java.text.SimpleDateFormat;
/**
 * Created by W10-PRO on 10/10/2016.
 */
public class Person {
    private String name;
    private Date birthday;

    public Person(String name, Date birthday){
        this.name = name;
        this.birthday = birthday;
    }

    public String getName(){
        return name;
    }

    public Date getDate(){
        return birthday;
    }

    public String toString(){
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        return (name + " " + format.format(birthday));
    }
}
