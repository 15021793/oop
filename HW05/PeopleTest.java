import people.Employee;

import java.util.Date;
/**
 * Created by W10-PRO on 10/10/2016.
 */
public class PeopleTest {
    public static void main(String[] args) {
        Employee newbie = new Employee
                ("Newbie", new Date("2/10/1989"), 1000000);
        Manager boss = new Manager
                ("Boss", new Date("2/23/1986"), 4000000);
        boss.setAssistant(newbie);
        Manager biggerBoss = new Manager
                ("Big Boss", new Date("3/12/1969"), 10000000);
        biggerBoss.setAssistant(boss);

        Object[] a = new Object[3];
        a[0] = newbie; a[1] = boss; a[2] = biggerBoss;
        for (int i=0 ; i<=2; ++i){
            System.out.println(a[i]);
        }

    }
}
