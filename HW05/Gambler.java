import java.util.Random;

/**
 * Created by W10-PRO on 10/10/2016.
 */
public class Gambler extends BankAccount{
    private  int fee;

    public Gambler(int n){
        super(n);
        fee = 0;
    }

    public boolean withdraw(int n){
        Random rd = new Random();
        int y; int x = rd.nextInt(100);
        if (x >= 49) {
            fee += 2*n;
            return super.withdraw(2*n);
        }
        else return super.withdraw(n);
    }

    public int endMonthCharge(){
        return fee;
    }
}
