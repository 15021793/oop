/**
 * Created by W10-PRO on 10/10/2016.
 */
public class NickelNDime extends BankAccount{

    public NickelNDime(int n){
        super(n);
    }

    public boolean withdraw(int n){
        return super.withdraw(n + 2000);
    }

    public int endMonthCharge(){
        return 2000*getNtransaction_withdraw();
    }

}
