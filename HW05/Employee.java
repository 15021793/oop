package people;

import people.Person;

import java.util.Date;
/**
 * Created by W10-PRO on 10/10/2016.
 */
public class Employee extends Person {
    private  double salary;

    public Employee(String name, Date birthday, double salary){
        super(name,birthday);
        this.salary = salary;
    }

    public double getSalary(){
        return salary;
    }

    public String getName(){
        return super.getName();
    }

    public Date getDate(){
        return super.getDate();
    }

    public String toString(){

        return (super.toString() + " " + salary);
    }

}
