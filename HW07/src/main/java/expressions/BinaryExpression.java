package expressions;

/**
 * Created by W10-PRO on 10/24/2016.
 */
public abstract class BinaryExpression extends Expression {

    abstract public Expression left();
    abstract public Expression right();
    abstract public int evaluate();
    abstract public String toString();
}
