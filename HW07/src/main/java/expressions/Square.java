package expressions;

/**
 * Created by W10-PRO on 10/24/2016.
 */
public class Square extends Expression {
    private Expression expression;

    public Square(Expression expression){
        this.expression = expression;
    }

    public int evaluate() {
        return expression.evaluate() * expression.evaluate();
    }

    public String toString(){
        return (expression + "^" + 2);
    }
}
