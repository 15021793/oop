package expressions;

/**
 * Created by W10-PRO on 10/24/2016.
 */
public abstract class Expression {
    public abstract int evaluate();
    public abstract String toString();
}
