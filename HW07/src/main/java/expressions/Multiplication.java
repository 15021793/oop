package expressions;

/**
 * Created by W10-PRO on 10/24/2016.
 */
public class Multiplication extends BinaryExpression {

    private Expression left;
    private Expression right;

    @Override
    public Expression left() {
        return this.left;
    }

    @Override
    public Expression right() {
        return this.right;
    }

    @Override
    public int evaluate() {
        return left.evaluate() * right.evaluate();
    }

    @Override
    public String toString() {
        return ("(" + left + " * " + right + ")");
    }
}
