package expressions;

/**
 * Created by W10-PRO on 10/24/2016.
 */
public class Addition extends BinaryExpression {

    private Expression left;
    private Expression right;

    public Addition(Expression left, Expression right) {
        this.left = left;
        this.right= right;
    }

    @Override
    public Expression left() {
        return this.left;
    }

    @Override
    public Expression right() {
        return this.right;
    }

    @Override
    public int evaluate() {
        return left.evaluate() + right.evaluate();
    }

    @Override
    public String toString() {
        return ("(" + left + " + " + right + ")");
    }
}
