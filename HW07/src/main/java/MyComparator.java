/**
 * Created by W10-PRO on 10/25/2016.
 */
public interface MyComparator {
    boolean less(BankAccount a1,BankAccount a2);
}
