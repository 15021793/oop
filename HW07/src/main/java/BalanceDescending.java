/**
 * Created by W10-PRO on 10/25/2016.
 */
public class BalanceDescending implements MyComparator {
    @Override
    public boolean less(BankAccount a1, BankAccount a2) {
        if (a1.getBalance() > a2.getBalance()) return true;
        return false;
    }
}
