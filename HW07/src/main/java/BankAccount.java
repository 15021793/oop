/**
 * Created by W10-PRO on 10/10/2016.
 */
public abstract class BankAccount {
    private  int balance;
    private  int Ntransaction;
    private  int Ntransaction_withdraw;

    public BankAccount(int b){
        balance = b;
        Ntransaction = 0;
        Ntransaction_withdraw = 0;
    }

    public boolean deposit(int n){
        balance += n;
        ++Ntransaction;
        return true;
    }

    public boolean withdraw(int n){
        if (n > balance) return false;

        balance -= n;
        ++Ntransaction_withdraw;
        ++Ntransaction;
        return true;
    }

    public int getBalance(){
        return balance;
    }

    public int getNtransaction(){
        return Ntransaction;
    }

    public int getNtransaction_withdraw(){
        return Ntransaction_withdraw;
    }

    public void resetTransaction(){
        Ntransaction_withdraw = 0;
        Ntransaction = 0;
    }

    public abstract int endMonthCharge();

    public void endMonth(){
        System.out.println(balance);
        System.out.println(Ntransaction);
        System.out.println(endMonthCharge());
        resetTransaction();
    }
}
