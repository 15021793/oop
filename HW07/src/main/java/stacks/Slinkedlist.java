package stacks;

/**
 * Created by W10-PRO on 10/25/2016.
 */
public class Slinkedlist implements StackOfString {
    private class Node{
        String string;
        Node next;

        private Node(String s, Node n){
            string = s;
            next = n;
        }
    }

    int size;
    private Node head = null;

    public Slinkedlist(){
        size = 0;
        head = null;
    }

    public boolean isEmpty(){
        return (size == 0);
    }

    public void push(String s){
        if (size == 0) {
            ++size;
            head = new Node(s,null);
            return;
        }
        ++size;
        Node tmp = new Node(s,head);
        head = tmp;
        return;
    }

    public String pop(){
        String tmp = head.string;
        head = head.next;
        --size;
        return tmp;
    }
}
