package stacks;

/**
 * Created by W10-PRO on 10/25/2016.
 */
public interface StackOfString {
    boolean isEmpty();
    void push(String s);
    String pop();
}
