package stacks;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by W10-PRO on 10/25/2016.
 */
public class Sarray implements StackOfString {

    private int size;
    private List<String> list = new ArrayList<>();

    public Sarray() {
        size = 0;
    }

    @Override
    public boolean isEmpty() {
        return (size == 0);
    }

    @Override
    public void push(String s) {
        list.add(s);
        ++size;
        return;
    }

    @Override
    public String pop() {
        String s = list.get(size-1);
        --size;
        return s;
    }
}
