/**
 * Created by W10-PRO on 10/25/2016.
 */
public class DemoClient {
    public static void main(String[] args) {
        BankAccount[] account = new BankAccount[3];
        account[0] = new FlatFee(15000);
        account[1] = new NickelNDime(10000);
        account[2] = new Gambler(70000);

        BalanceDescending compare = new BalanceDescending();

        InsertionSort a = new InsertionSort(account,compare);
        a.sort();
        account = a.getAccount();

        for (int i=0; i<= account.length-1; ++i)
            System.out.println(account[i].getBalance());

    }
}
