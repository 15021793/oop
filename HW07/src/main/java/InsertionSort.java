/**
 * Created by W10-PRO on 10/25/2016.
 */
public class InsertionSort {
    private BankAccount[] account;
    private MyComparator compare;

    public InsertionSort(BankAccount[] account, MyComparator compare) {
        this.account = account;
        this.compare = compare;
    }

    public BankAccount[] getAccount() {
        return account;
    }

    public void sort() {
        int j;
        for (int i=1; i<=account.length-1; ++i) {
            j = i;
            while (j >=1 && !compare.less(account[j-1],account[j])) {
                BankAccount tmp;
                tmp = account[j-1];
                account[j-1] = account[j];
                account[j] = tmp;
                --j;
            }
        }
    }

}
