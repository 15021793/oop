/**
 * Created by W10-PRO on 10/25/2016.
 */
public class TransactionCountDescending implements MyComparator {
    @Override
    public boolean less(BankAccount a1, BankAccount a2) {
        if (a1.getNtransaction() > a2.getNtransaction()) return true;
        return false;
    }
}
